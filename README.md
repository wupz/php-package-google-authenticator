### Google Authenticator PHP Library

- command
```
composer require wupz/google-authenticator
```

- example
```php
$gc = new wuweiit\GoogleAuthenticator();
/**
 * 生成二维码
 */
$secret = $gc->createSecret(16);
echo 'secret:' . $secret . PHP_EOL;
echo 'qrurl:' . $gc->getQRCodeGoogleUrl('吴强强', $secret, $title = 'wuweiit', $params = array()) . PHP_EOL;


/**
 * 验证Code
 */
$code = $gc->getCode($secret);
echo 'code:' . $code . PHP_EOL;
echo 'status:' . $gc->verifyCode($secret , $code);
```